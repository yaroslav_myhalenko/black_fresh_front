const argv = require('minimist')(process.argv);
const is_production = argv.production || false;

// gulp modules and helpers
const { src, dest, parallel, series, watch } = require('gulp');
const filter = require('gulp-filter');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const changed = require('gulp-changed');
const del = require('del');
const browserSync = require('browser-sync').create();

// html plugins
const htmlmin = require('gulp-htmlmin');

// postcss plugins
const autoprefixer = require('autoprefixer');
const postcss = require('gulp-postcss');
const postcssImport = require('postcss-import');
const postcssProperties = require('postcss-custom-properties');
const postcssCalc = require('postcss-calc');
const postcssNesting = require('postcss-nesting');
const postcssMedia = require('postcss-custom-media');
const postcssImageSet = require('postcss-image-set-function');
const postcssCsso = require('postcss-csso');

// image plugins
const imagemin = require('gulp-imagemin');
const webp = require('gulp-webp');

// js plugins
const uglify = require('gulp-uglify');

// tasks below
function clean() {
    return del([
        'build/*',
    ]);
}

function html() {
    const options = {
        collapseBooleanAttributes: true,
        collapseWhitespace: true,
        removeComments: true,
        removeEmptyAttributes: true,
        removeScriptTypeAttributes: true,
        removeStyleLinkTypeAttributes: true
        // minifyCSS: true,
        // minifyJS: true
    };
    
    return src('src/*.html')
      .pipe(changed('build'))
      // .pipe(htmlmin(options))
      .pipe(dest('build/'));
}

function css() {
    // plugins order is important
    const plugins = [
        postcssImport(),
        postcssProperties(),
        postcssCalc(),
        postcssNesting(),
        postcssMedia(),
        postcssImageSet(),
        autoprefixer(),
        postcssCsso(),
    ];
    
    return src('src/css/style.css', { sourcemaps: is_production ? false : true })
      .pipe(postcss(plugins))
      .pipe(rename({ extname: '.min.css' }))
      .pipe(dest('build/css', { sourcemaps: is_production ? false : true }));
}

function images() {
    return src('src/images/*')
      .pipe(changed('build/images'))
      .pipe(imagemin())
      .pipe(dest('build/images'))
      .pipe(filter('**/*.{jpg,png}'))
      .pipe(rename({ extname: '.webp' }))
      .pipe(webp({
        quality: 80
      }))
      .pipe(dest('build/images'));
}

function fonts() {
    return src('src/fonts/*')
      .pipe(changed('build/fonts'))
      .pipe(dest('build/fonts'));
}

function js() {
    const options = {
        mangle: {
            reserved: ['$', 'require', 'exports']
        }
    };

    const paths = [
        'src/js/vendors/jquery-3.4.1.min.js',
        'src/js/vendors/plugins.js',
        'src/js/common.js',
        'src/js/*.js',
    ];
    
    return src(paths, { sourcemaps: is_production ? false : true })
      .pipe(concat('app.min.js'))
      .pipe(src(['src/js/vendors/modernizr.min.js'], { sourcemaps: is_production ? false : true }))
      .pipe(uglify(options))
      .pipe(dest('build/js', { sourcemaps: is_production ? false : true }));
}

function serve(done) {
    browserSync.init({
        server: {
            baseDir: './build'
        }
    });
    browserSync.watch('build/**/*').on('change', browserSync.reload);
    done();
}

function watcher() {
    watch('src/*.html', html);
    watch('src/css/**/*.css', css);
    watch('src/images/*', images);
    watch('src/fonts/*', fonts);
    watch('src/js/**/*.js', js);
}

const build = series(clean, parallel(html, css, images, fonts, js));

// single tasks
exports.clean = clean;
exports.html = html;
exports.css = css;
exports.images = images;
exports.fonts = fonts;
exports.js = js;
exports.serve = serve;

// complex tasks
exports.build = build;
exports.default = parallel(watcher, serve); // series(build, parallel(watcher, serve));