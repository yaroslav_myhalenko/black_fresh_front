$(function() {
    if ($('#registration').length) {
        $('.course__registration').on('click', 'a', function(e) {
            e.preventDefault();

            $.scrollTo($('#registration').offset().top - 30 - $('.header').height(), 300, {easing: 'easeOutQuad'});
        });

        $('#registration-phone').inputmask({
            mask: '+380 99 999 9999',
            showMaskOnHover: false,
            showMaskOnFocus: true
        });

        var is_course_xhr = false;

        $('#registration').on('submit', 'form', function(e) {
            e.preventDefault();
            document.activeElement.blur();
        
            if (is_course_xhr) {
              return false;
            }
        
            is_course_xhr = true;
            form = $(this);
        
            $.ajax({
                type: 'POST',
                url: form.attr('action'),
                data: form.serialize(),
                cache: false,
                dataType: 'json',
                success: function(data) {
                    is_course_xhr = false;
            
                    // form.removeClass('form-info--success');
                    form.find('.form__input--error').removeClass('form__input--error');
                    form.find('.form__error').remove();
            
                    if (data.error == true) {
                        if (data.errorCode != '') {
                            form.find('button').after('<div class="form__error">' + data.errorCode + '</div>');
                        } else {
                            for (field in data.errorFields) {
                                form.find('[name="registration[' + field + ']"]').each(function() {
                                    if (this.type == 'hidden') {
                                        return;
                                    } else if (this.type == 'checkbox' || this.type == 'radio') {
                                        $(this).addClass('form__input--error').parent().parent().append('<div class="form__error">' + data.errorFields[field] + '</div>');
                                    } else {
                                        $(this).addClass('form__input--error').parent().append('<div class="form__error">' + data.errorFields[field] + '</div>');
                                    }
                                });
                            }
                        }
                    } else {
                        form.html(data.msg);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    is_course_xhr = false;
                    alert('Request error!');
          
                    // console.log('Loader Error:\n' + textStatus + ' ' + errorThrown );
                }
            });
        });
    }
});