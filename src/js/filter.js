$(function() {
	if ($('#filter').length) {
		var filter = $('#filter'),
			filter_total = filter.find(':checked').length,
			filter_toggle = $('.filter-toggle'),
			filter_toggle_text = filter_toggle.find('.filter-toggle__text'),
			filter_toggle_counter = filter_toggle.find('.filter-toggle__counter'),
			filter_show_text = filter_toggle_text.text(),
			filter_hide_text = filter_toggle.data('hide-text'),
			filter_reset_counter = $('.filter__reset-counter'),
			dropdownChanged = function(dropdown) {
				var dropdown_total = dropdown.find(':checked').length;

				if (dropdown_total) {
					dropdown.find('.filter-dropdown__value span').text('(' + dropdown_total + ')');
				} else {
					dropdown.find('.filter-dropdown__value span').empty();
				}
			},
			totalChanged = function() {
				if (filter_total) {
					filter_toggle_counter.text('(' + filter_total + ')').removeClass('filter-toggle__counter--hidden');
					filter_reset_counter.text('(' + filter_total + ')').parent().removeClass('filter__reset--hidden');
				} else {
					filter_toggle_counter.empty().addClass('filter-toggle__counter--hidden');
					filter_reset_counter.empty().parent().addClass('filter__reset--hidden');
				}
            },
            closeDropdown = function(el) {
                var namespace = el.data('namespace');
                
                el.find('.filter-dropdown__list').one(transition_event, function() {
                    el.removeClass('filter-dropdown--active');
                }).css('maxHeight', '');
                el.removeClass('filter-dropdown--open');
                el.find('.filter-dropdown__arrow').off('click.dropdown' + namespace);
                d.off('click.dropdown' + namespace);
            },
            dropdownToggle = function(e) {
                e.preventDefault();
                this.blur();
        
                var el = $(this).parent(),
                    namespace = el.data('namespace'),
                    max_height = el.data('max-height');
        
                if (!namespace) {
                    namespace = Date.now();
                    el.data('namespace', namespace);
                }
        
                if (!max_height) {
                    max_height = el.find('.filter-dropdown__list').outerHeight();
                    el.addClass('filter-dropdown--init').data('max-height', max_height);
                    el[0].offsetHeight;
                }
        
                if (el.hasClass('filter-dropdown--open')) {
                    closeDropdown(el);
                } else {
                    el.find('.filter-dropdown__list').css('maxHeight', max_height);
                    el.addClass('filter-dropdown--open filter-dropdown--active');
        
                    // disable click on document for mobile filters
                    if (!is_sm_screen.matches) {
                        el.find('.filter-dropdown__arrow').on('click.dropdown' + namespace, function(e) {
                            e.stopPropagation();
                            closeDropdown(el);
                        });
                        
                        d.on('click.dropdown' + namespace, function(e) {
                            if (!el.is(e.target) && el.has(e.target).length === 0) {
                                closeDropdown(el);
                            }
                        });
                    }
                }
            };

		filter.on('change', 'input[type="checkbox"]', function(e) {
			if ($(this).is(':checked')) {
				filter_total++;
			} else {
				filter_total--;
			}

			dropdownChanged($(this).parents('.filter-dropdown'));
            totalChanged();
            filter.submit();
		});
		
		filter_toggle.on('click', function(e) {
			e.preventDefault();
			this.blur();

			if ($(this).hasClass('filter-toggle--open')) {
				filter_toggle_text.text(filter_show_text);
			} else {
				filter_toggle_text.text(filter_hide_text);
			}

			$(this).toggleClass('filter-toggle--open');
        });
        
        $('.filter-dropdown:not(.filter-filter-dropdown--phone)').on('click', '.filter-dropdown__value', dropdownToggle);
	}
});