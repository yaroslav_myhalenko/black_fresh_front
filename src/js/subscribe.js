$(function() {
    $('#subscribe').on('submit', function(e) {
        e.preventDefault();

        var form = $(this),
            params = form.serialize();

        form.find('input').prop('disabled', true);

        $.ajax({
            type: 'POST', 
            url: form.attr('action'), 
            data: params, 
            cache: false, 
            dataType: 'json',
            success: function(data) {
                form.find('input').prop('disabled', false);
                form.find('.form__error').remove();

                if (data.error) {
                    if (data.errorCode != '') {
                        form.find('input').parent().append('<div class="form__error">' + data.errorCode + '</div>');
                    } else {
                        for (field in data.errorFields) {
                            form.find('[name="subscribe[' + field + ']"]').each(function() {
                                if (this.type == 'hidden') {
                                    return;
                                } else if (this.type == 'checkbox' || this.type == 'radio') {
                                    $(this).addClass('form__input--error').parent().parent().append('<div class="form__error">' + data.errorFields[field] + '</div>');
                                } else {
                                    $(this).addClass('form__input--error').parent().append('<div class="form__error">' + data.errorFields[field] + '</div>');
                                }
                            });
                        }
                    }
                } else {
                    form.find('input').parent().html('<b>' + data.message + '</b>');
                }
            },
            error: function(jqXHR, textStatus, errorThrown) { 
                form.find('input').prop('disabled', false);
                form.find('.form__error').remove();
                
                // console.log('Loader Error:\n' + textStatus + ' ' + errorThrown );
            }
        });
    });
});