$(function() {
    if ($('#checkout').length) {
		var checkout = $('#checkout'),
			checkout_np = $('#checkout-np'),
            checkout_courier = $('#checkout-courier'),
            np_department = $('#checkout-np-department'),
            np_details = $('#np-details'),
            np_details_default = np_details.text(),
			is_checkout_xhr = false,
            checkoutAction = function(e) {
                e.preventDefault();
                document.activeElement.blur();

                if (is_checkout_xhr) {
                    return false;
                }

                is_checkout_xhr = true;
                checkout.addClass('checkout--loading');

                var form = $(this);

                $.ajax({
                    type: 'POST', 
                    url: form.attr('action'), 
                    data: form.serialize(), 
                    cache: false, 
                    dataType: 'json',
                    success: function(data) {
                        checkout.removeClass('checkout--loading');
                        is_checkout_xhr = false;

                        form.find('.form__input--error').removeClass('form__input--error');
                        form.find('.form__error').remove();
                        
                        if (data.error == true) {
                            if (data.errorCode != '') {
                                form.find('button').after('<div class="form__error">' + data.errorCode + '</div>');
                            } else {
                                for (field in data.errorFields) {
                                    form.find('[name="order[' + field + ']"]').each(function() {
                                        if (this.type == 'hidden') {
                                            return;
                                        } else if (this.type == 'checkbox' || this.type == 'radio') {
                                            $(this).addClass('form__input--error').parent().parent().append('<div class="form__error">' + data.errorFields[field] + '</div>');
                                        } else {
                                            $(this).addClass('form__input--error').parent().append('<div class="form__error">' + data.errorFields[field] + '</div>');
                                        }
                                    });
                                }
                            }

                            $.scrollTo($('.form__error:first').parent().parent().offset().top - 20, 300, {easing: 'easeOutQuad'});
                        } else {
                            if (data.thankYou) {
                                window.location = data.thankYou;
                            } else if (data.paymentForm) {
                                $('body').append('<div id="pay-form" style="overflow:hidden;height:0">' + data.paymentForm + '</div>');
                                $('#pay-form').find('form').submit();
                            }
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) { 
                        checkout.removeClass('checkout--loading');
                        is_checkout_xhr = false;

                        form.find('.form__input--error').removeClass('form__input--error');
                        form.find('.form__error').remove();
                        
                        form.find('button').after('<div class="form__error">Request error!</div>');
                        
                        // console.log('Loader Error:\n' + textStatus + ' ' + errorThrown );
                    }
                });
            };
		
		checkout.on('change', '.form__input--error', function() {
			$(this).removeClass('form__input--error').next('.form__error').remove();
        });
        
        $('#checkout-phone').inputmask({
            mask: '+380 99 999 9999',
            showMaskOnHover: false,
            showMaskOnFocus: true
        });
		
		checkout.on('change', 'input[name="order[delivery]"]', function() {
			var delivery_option = $(this).val();

			if (delivery_option == 1) {
                checkout_courier.addClass('checkout__delivery-group--hidden').find(':input').prop('disabled', true);
                checkout_np.addClass('checkout__delivery-group--hidden'); // .find(':input').prop('disabled', true);
                $('#checkout-np-city')[0].selectize.disable();
                np_department[0].selectize.disable();
            } else if (delivery_option == 2) {
                checkout_courier.removeClass('checkout__delivery-group--hidden').find(':input').prop('disabled', false);
                checkout_np.addClass('checkout__delivery-group--hidden'); // .find(':input').prop('disabled', true);
                $('#checkout-np-city')[0].selectize.disable();
                np_department[0].selectize.disable();
            } else if (delivery_option == 3) {
                checkout_courier.addClass('checkout__delivery-group--hidden').find(':input').prop('disabled', true);
                checkout_np.removeClass('checkout__delivery-group--hidden'); // .find(':input').prop('disabled', false);
                $('#checkout-np-city')[0].selectize.enable();

                // if ($('#checkout-np-department').find('option').length > 1) {
                if (Object.keys(np_department[0].selectize.options).length > 1) {
                    // $('#checkout-np-department').prop('disabled', false);
                    np_department[0].selectize.enable();
                } else {
                    // $('#checkout-np-department').prop('disabled', true);
                    np_department[0].selectize.disable();
                }
            }
		});

		var city_xhr = {
            loading: false,
            xhr: null
        };

        checkout.on('submit', checkoutAction);
        
        $('#checkout-np-city').removeClass('form__input').selectize({
            highlight: false,
            maxOptions: 2000,
            create: false,
            valueField: 'value',
            labelField: 'text',
            searchField: 'text',
            onChange: function(value) {
                if (city_xhr.loading) {
                    city_xhr.loading = false;
                    city_xhr.xhr.abort();
                    city_xhr.xhr = null;
                }

                np_department[0].selectize.clear(true);
                np_department[0].selectize.clearOptions();
                np_department[0].selectize.refreshOptions(false);
                
                if (value == '') {
                    np_department[0].selectize.disable();

                    $('#np-details').text(np_details_default);
                } else {
                    city_xhr.loading = true;
                    city_xhr.xhr = $.ajax({type: "GET", url: checkout.data('baseurl') + '/ajax/npdepartments', data: $.param({city: value}), cache: false, dataType: "json", 
                        success: function(data) {
                            city_xhr.loading = false;
                            city_xhr.xhr = null;

                            np_department[0].selectize.enable();
                            np_department[0].selectize.addOption(data.options);
                            np_department[0].selectize.refreshOptions(true);

                            $('#np-details').text(data.cost ? data.cost : np_details_default);
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            city_xhr.loading = false;
                            city_xhr.xhr = null;

                            np_department[0].selectize.enable();
                            $('#np-details').text(np_details_default);

                            //console.log('Loader Error:\n' + errorThrown );
                        }
                    });

                    // set np details
                }
            }
        });

        np_department.removeClass('form__input').selectize({
            highlight: false,
            maxOptions: 2000,
            create: false,
            valueField: 'value',
            labelField: 'text',
            searchField: 'text'
        });

        checkout.find('input[name="order[delivery]"]:checked').change();
        
        checkout.on('change', '#agree', function() {
            if ($(this).is(':checked')) {
                checkout.find('button').prop('disabled', false);
            } else {
                checkout.find('button').prop('disabled', true);
            }
        });
        $('#agree').change();
	}
});