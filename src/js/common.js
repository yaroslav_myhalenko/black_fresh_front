// helpers and global variables
var d,
	h,
	b,
	c,
	scrollbar,
	cart_scrollbar,
	scrollbar_width = 0,
	screen_width = 0,
	is_retina = false,
	is_mobile = false,
	is_mobile_chrome = false,
	is_form_xhr = false,
	form_xhr,
	slider_opts = {
		preloadImages: false,
		preventClicks: true,
		simulateTouch: false,
		loop: true,
		speed: 400
	},
	is_sm_screen = window.matchMedia("(max-device-width: 767px)"),
	has_custom_scrollbar = Modernizr.is_mobile && is_sm_screen.matches ? false : true,
	loadScript = function(src, callback, charset) {
		var s,
			r,
			t;

		r = false;
		s = document.createElement('script');
		s.type = 'text/javascript';
		s.async = true;
		s.src = src;
		
		if(typeof charset !== 'undefined')
			s.charset = charset;
		
		s.onload = s.onreadystatechange = function() {
			//console.log( this.readyState ); //uncomment this line to see which ready states are called.
			if ( !r && (!this.readyState || this.readyState == 'complete') )
			{
				r = true;
				callback();
			}
		};
		
		document.body.appendChild(s);
	},
	getScrollbarWidth = function() {
		// Create the measurement node
		var scrollDiv = document.createElement("div");
		scrollDiv.style.width = '100px';
		scrollDiv.style.height = '100px';
		scrollDiv.style.overflow = 'scroll';
		scrollDiv.style.position = 'absolute';
		scrollDiv.style.top = '-9999px';
		document.body.appendChild(scrollDiv);

		// Get the scrollbar width
		var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;

		// Delete the DIV 
		document.body.removeChild(scrollDiv);

		return scrollbarWidth;
	},
	isRetina = function() {
		var mediaQuery = '(-webkit-min-device-pixel-ratio: 1.5), (min-resolution: 1.5dppx)';

		if (window.devicePixelRatio > 1) {
			return true;
		}

		if (window.matchMedia && window.matchMedia(mediaQuery).matches) {
			return true;
		}

		return false;
	},
	transition_event,
	whichTransitionEvent = function() {
		var t,
			el = document.createElement("fakeelement");

		var transitions = {
			"transition"      : "transitionend",
			// "OTransition"  : "oTransitionEnd",
			"MozTransition"   : "transitionend",
			"WebkitTransition": "webkitTransitionEnd"
		}

		for (t in transitions){
			if (el.style[t] !== undefined){
				return transitions[t];
			}
		}
	},
	viewportChanged = function() {
		screen_width = b.width() + scrollbar_width;
		
		var vw = c.width(),
			vh = $(window).height();
		
		// set custom properties
		if (Modernizr.is_mobile) { // Modernizr.is_mobile_chrome
			document.documentElement.style.setProperty('--vh', vh + 'px');
		}

		document.documentElement.style.setProperty('--vw', vw + 'px');
	},
	setCookie = function(name, value, options) {
		options = options || {};

		var expires = options.expires;

		if (typeof expires == "number" && expires) {
			var d = new Date();
			d.setTime(d.getTime() + expires * 1000);
			expires = options.expires = d;
		}
		
		if (expires && expires.toUTCString) {
			options.expires = expires.toUTCString();
		}

		value = encodeURIComponent(value);

		var updatedCookie = name + "=" + value;

		for (var propName in options) {
			updatedCookie += "; " + propName;
			var propValue = options[propName];
			
			if (propValue !== true) {
				updatedCookie += "=" + propValue;
			}
		}

		document.cookie = updatedCookie;
	},
	scroll_locked = false,
	scroll_top = 0,
	toggleScrollLock = function() {
		if (Modernizr.is_mobile) {
			if (b.hasClass('is-locked')) {
				b.removeClass('is-locked');
				$(window).scrollTop(scroll_top);
				
				// small delay for scroll events
				setTimeout(function() {
					scroll_locked = false;
				}, 100);
			} else {
				scroll_top = $(window).scrollTop();
				scroll_locked = true;

				b.addClass('is-locked').css({
					top: scroll_top ? -scroll_top : 0
				});
			}
		} else {
			if (b.hasClass('is-locked')) {
				b.removeClass('is-locked').css({
					overflow: '',
					marginRight: ''
				});
			} else {
				b.addClass('is-locked').css({
					overflow: 'hidden',
					marginRight: scrollbar_width
				});
			}
		}
	},
	number_format = function(number, decimals, decPoint, thousandsSep) {
		number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
		var n = !isFinite(+number) ? 0 : +number
		var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
		var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
		var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
		var s = ''

		var toFixedFix = function (n, prec) {
			var k = Math.pow(10, prec)
			return '' + (Math.round(n * k) / k)
				.toFixed(prec)
		}

		// @todo: for IE parseFloat(0.55).toFixed(0) = 0;
		s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
		if (s[0].length > 3) {
			s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
		}
		if ((s[1] || '').length < prec) {
			s[1] = s[1] || ''
			s[1] += new Array(prec - s[1].length + 1).join('0')
		}

		return s.join(dec)
	};

$.easing.easeOutQuad = function (x, t, b, c, d) {
	return -c *(t/=d)*(t-2) + b;
};

$(function() {
	d = $(document);
	h = $('html');
	b = $('body');
	c = $('.container');
	scrollbar_width = getScrollbarWidth();
	transition_event = whichTransitionEvent();
	screen_width = b.width() + scrollbar_width;

	viewportChanged();

	$('#dropdown').find('.dropdown__menu').on('mouseenter', 'a', function(e) {
		var index = $(this).parent().index();

		$('.dropdown__section:eq(' + index + ')').addClass('dropdown__section--visible')
			.siblings().removeClass('dropdown__section--visible');
	});

	var header = $('.header');
	var mobile_menu = $('.mobile-menu');
	var toggleMenu = function(el) {
		if (mobile_menu.hasClass('mobile-menu--hidden')) {
			el.addClass('header__menu-toggle--open');
			mobile_menu.removeClass('mobile-menu--hidden');
			header.addClass('header--menu-open');

			d.on('click.mobile-menu', function(e) {
				if (!el.is(e.target) && !mobile_menu.is(e.target) && mobile_menu.has(e.target).length === 0) {
					toggleMenu(el);
				}
			});
		} else {
			el.removeClass('header__menu-toggle--open');
			header.removeClass('header--menu-open');
			mobile_menu.addClass('mobile-menu--hidden');

			d.off('click.mobile-menu');
		}
	};
	
	$('.header__menu-toggle').on('click', function(e) {
		e.preventDefault();
		var el = $(this);

		toggleMenu(el);
	});

	if (Modernizr.is_mobile) {
		var catalog = $('.header__catalog');
		var toggleCatalog = function(el) {
			if (!catalog.hasClass('header__catalog--open')) {
				header.addClass('header--catalog-open');
				catalog.addClass('header__catalog--open');
	
				d.on('click.catalog', function(e) {
					if (!el.is(e.target) && !catalog.is(e.target) && catalog.has(e.target).length === 0) {
						toggleCatalog(el);
					}
				});
			} else {
				header.removeClass('header--catalog-open');
				catalog.removeClass('header__catalog--open');
	
				d.off('click.catalog');
			}
		};
		
		$('.header__catalog-btn').on('click', function(e) {			
			e.preventDefault();
			var el = $(this);
	
			toggleCatalog(el);
		});
	} else {
		$('.header__catalog-btn').on('click', function(e) {
			e.preventDefault();
		});
	}

	$('.header__lang-active').on('click', function(e) {
		e.preventDefault();
	});

	$(window).on('resize.window', $.throttle(50, viewportChanged));
});