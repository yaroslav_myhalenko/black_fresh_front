$(function() {
    $('#discount')
    .on('change', 'input', function() {
        var value = $(this).val();

        if ($.trim(value) != '') {
            $('#discount').submit();
        }
    })  
    .on('input', 'input', function() {
        $('#discount').find('.form__input--error').removeClass('form__input--error');
        $('#discount').find('.form__error').remove();
        $('.checkout__delivery--discount').remove();
    }) 
    .on('keydown', 'input', function(e) {
        if (e.which == 13) {
            e.preventDefault();
            $(this).blur();
        }
    })
    .on('submit', function(e) {
        e.preventDefault();

        var form = $(this),
            params = form.serialize();

        if ($.trim(form.find('input').val()) == '') {
            return false;
        }

        form.find('input').prop('disabled', true);

        $.ajax({
            type: 'POST', 
            url: form.attr('action'), 
            data: params, 
            cache: false, 
            dataType: 'json',
            success: function(data) {
                form.find('input').prop('disabled', false);
                form.find('.form__input--error').removeClass('form__input--error');
                form.find('.form__error').remove();
                $('.checkout__delivery--discount').remove();

                if (data.error) {
                    if (data.errorCode != '') {
                        form.find('input').after('<div class="form__error">' + data.errorCode + '</div>');
                    } else {
                        for (field in data.errorFields) {
                            form.find('[name="discount[' + field + ']"]').each(function() {
                                if (this.type == 'hidden') {
                                    return;
                                } else if (this.type == 'checkbox' || this.type == 'radio') {
                                    $(this).addClass('form__input--error').parent().parent().append('<div class="form__error">' + data.errorFields[field] + '</div>');
                                } else {
                                    $(this).addClass('form__input--error').parent().append('<div class="form__error">' + data.errorFields[field] + '</div>');
                                }
                            });
                        }
                    }
                } else {
                    form.find('input').val('');
                    form.after('<div class="checkout__delivery checkout__delivery--discount">' + 
                        '<span>' + data.message + ' <b>' + data.code + '</b></span>' + 
                        '<span>' + data.value + ' грн</span>' + 
                    '</div>');
                    $('.checkout__total').find('span:last').text(data.price + ' грн');
                }
            },
            error: function(jqXHR, textStatus, errorThrown) { 
                form.find('input').prop('disabled', false);
                form.find('.form__input--error').removeClass('form__input--error');
                form.find('.form__error').remove();
                $('.checkout__delivery--discount').remove();
                
                // console.log('Loader Error:\n' + textStatus + ' ' + errorThrown );
            }
        });
    });
});