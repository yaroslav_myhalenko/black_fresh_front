$(function() {
    if ($('#banners').length) {
        var banners = $('#banners');

        new Swiper(banners.children('.swiper-container')[0], $.extend({}, slider_opts, {
            slidesPerView: 1,
            slidesPerGroup: 1,
            spaceBetween: 0,
            simulateTouch: false,
            loop: true,
            autoplay: {
                delay: 5000,
                disableOnInteraction: false
            },
            pagination: {
                el: banners.find('.swiper-pagination')[0],
                clickable: true
            },
            navigation: {
                prevEl: banners.find('.swiper-button-prev')[0],
                nextEl: banners.find('.swiper-button-next')[0]
            }
        }));
    }
    
    new Swiper('#instagram-slider', $.extend({}, slider_opts, {
        slidesPerView: 'auto',
        spaceBetween: 10,
        simulateTouch: true,
        loop: true,
        freeMode: false,
        speed: 600,
        autoplay: {
            delay: 3000,
        }
    }));
});