$(function() {
    if ($('#product-gallery').length) {
        var product_gallery = $('#product-gallery');

        new Swiper(product_gallery.children('.swiper-container')[0], $.extend({}, slider_opts, {
            slidesPerView: 1,
            slidesPerGroup: 1,
            spaceBetween: 10,
            simulateTouch: false,
            loop: false,
            pagination: {
                el: product_gallery.find('.swiper-pagination')[0],
                clickable: true
            },
            navigation: {
                prevEl: product_gallery.find('.swiper-button-prev')[0],
                nextEl: product_gallery.find('.swiper-button-next')[0]
            }
        }));
    }

    var total_options = $('.product__buy-section--option').length,
        optionChanged = function() {
            var total_changed = 0;

            $('.product__buy-section--option').each(function() {
                var value = '';
                
                if ($(this).find('select').length) {
                    value = $(this).find('select').val();
                } else if ($(this).find(':checked').length) {
                    value = $(this).find(':checked').val();
                }

                if (value != '') {
                    total_changed++;
                }
            });

            console.log(total_changed, total_options);
            if (total_changed == total_options) {
                $('.product__btn').prop('disabled', false);
            } else {
                $('.product__btn').prop('disabled', true);
            }
        }

    $('.product__select').on('change', function() {
        var value = $(this).find(':selected').text();

        $(this).prev().text(value);
        optionChanged();
    });

    $('.product__radio').on('change', 'input', optionChanged);

    setTimeout(function() {
        $('.product__select').change();
    }, 100);

    $('.product__qty-btns').on('change', '[type="radio"]', function() {
        $('#product-qty').val($(this).val());
        $('.product__input').val('');
    });

    $('.product__input').on('change', function() {
        var value = parseInt($(this).val(), 10);

        if (isNaN(value) || value < 1) {
            value = 1;
            $(this).val(value);
        }

        $('.product__qty-btns').find(':checked').prop('checked', false);
        $('#product-qty').val(value);
    });

    $('#product').on('submit', 'form', addToCart);

    if ($('#product-variant').length) {
        var product_price = $('.product__price');
        
        $('#product-variant').on('change', 'input[type="radio"]', function() {
            var price = $(this).parent().data('price');
            var price_old = $(this).parent().data('price-old');

            product_price.children('span').text(price);

            if (price_old != 0) {
                product_price.children('s').show().children('span').text(price_old);
            } else {
                product_price.children('s').hide().children('span').text(price_old);
            }
        });
    }

    if ($('#product-variants').length) {
        var product = $('#product'),
            product_variants = $('#product-variants'),
            product_variants_select = product_variants.find('input'),
            product_variants_selected = 0,
            product_variants_reset = product_variants.find('.buy__variants-reset'),
            variants = product_variants.data('variants'),
            variants_lang = product_variants.data('lang'),
            variant_active,
            price_el = product.find('.product__price span'),
            product_buy_form = $('.product__buy'),
            product_buy_btn = product_buy_form.find('button'),
            variant_el = $('#pb-variant'),
            price_base = price_el.data('price'),
            product_buy_url = product_buy_form.attr('action'),
            product_id = product.data('id'),
            product_url = window.location.href,
            product_base_url = product.data('url'),
            enableBuyForm = function() {
                product_buy_btn.prop('disabled', false);
            },
            disableBuyForm = function() {
                product_buy_btn.prop('disabled', true);
            },
            replaceProductUrl = function() {
                var active_url,
                    active_url_params = {};
                
                if (variant_active) {
                    active_url_params.vid = variant_active.variant_id;
                }

                if (variant_active) {
                    active_url = product_base_url + '?' + $.param(active_url_params);
                } else {
                    active_url = product_base_url;
                }

                if (product_url != active_url) {
                    product_url = active_url;
                    // window.history.replaceState({}, document.title, product_url);
                }
            };
        
        product_variants_reset.on('click', 'a', function(e) {
            e.preventDefault();

            product_variants_select.prop('checked', false).first().trigger('change');

            /* if (!is_default_product_gallery) {
                is_default_product_gallery = true;

                product_gallery.html(product_gallery_html);

                if (product_gallery_html != '') {
                    product_gallery.children().fotorama($.extend({}, fotorama_config, {
                        maxwidth: 560,
                        ratio: 580/400,
                        fit: 'contain'
                    }));
                }
            } */
        });

        product_variants.on('change', 'input', function() {
            // get selected values
            var selected_values = [],
                selected_properties = {};

            variant_active = null;

            product_variants_select.each(function() {
                if ($(this).val() == '' || $(this).prop('checked') == false) {
                    return;
                }

                var value_id = parseInt($(this).val(), 10),
                    property_id = $(this).parents('.buy__option').data('property');

                selected_values.push(value_id);
                selected_properties[property_id] = value_id;
            });

            if (selected_values.length) {
                product_variants_selected = selected_values.length;
                product_variants_reset.removeClass('hidden');

                selected_values.sort(function(a, b) {
                    return a - b;
                });

                var active_properties = {},
                    total_selected = selected_values.length;

                // collect values for selected options
                for (selected_property_id in selected_properties) {
                    var selected_property_values = $.extend({}, selected_properties);

                    delete selected_property_values[selected_property_id];

                    for (variant_index in variants) {
                        var selected_counter = 0;

                        for (property_id in selected_property_values) {
                            var value_id = selected_property_values[property_id];

                            if (variants[variant_index].values[property_id] == value_id) {
                                selected_counter++;
                            }
                        }

                        if (selected_counter == (total_selected - 1)) {
                            for (property_id in variants[variant_index].values) {
                                if (property_id != selected_property_id) {
                                    continue;
                                }

                                var value_id = variants[variant_index].values[property_id];
                                
                                if (!active_properties.hasOwnProperty(property_id)) {
                                    active_properties[property_id] = {};
                                }

                                active_properties[property_id][value_id] = value_id;
                            }
                        }
                    }
                }

                // show options from matched variants
                for (variant_index in variants) {
                    var selected_counter = 0;

                    for (property_id in selected_properties) {
                        var value_id = selected_properties[property_id];

                        if (variants[variant_index].values[property_id] == value_id) {
                            selected_counter++;
                        }
                    }

                    if (selected_counter == total_selected) {
                        for (property_id in variants[variant_index].values) {
                            if (selected_properties.hasOwnProperty(property_id)) {
                                continue;
                            }

                            var value_id = variants[variant_index].values[property_id];
                            
                            if (!active_properties.hasOwnProperty(property_id)) {
                                active_properties[property_id] = {};
                            }

                            active_properties[property_id][value_id] = value_id;
                        }
                    }
                }

                product_variants_select.prop('disabled', true);

                for (property_id in active_properties) {
                    for (value_id in active_properties[property_id]) {
                        product_variants.find('input[value="' + value_id + '"]').prop('disabled', false);
                    }
                }

                // selected variant
                var variant_key = selected_values.join('_');

                if (variants.hasOwnProperty(variant_key)) {
                    variant_active = variants[variant_key];

                    variant_el.val(variants[variant_key].variant_id);
                    price_el.text(number_format(variants[variant_key].price, 0, '.', ' '));

                    /* // set current sku
                    if (variant_active.sku != '') {
                        if (product_title_el.children('span').length) {
                            product_title_el.children('span').text(variants_lang.productCode + ' ' + variant_active.sku);
                        } else {
                            product_title_el.append(' <span>' + variants_lang.productCode + ' ' + variant_active.sku + '</span>');
                        }
                    } else {
                        product_title_el.children('span').remove();
                    } */

                    /*
                    // setup gallery
                    if (variants[variant_key].photos.length) {
                        is_default_product_gallery = false;

                        var gallery_html = '<div class="fotorama" data-auto="false">';

                        for (photo_index in variants[variant_key].photos) {
                            var large_photo = product_gallery_assets + '/product/' + product_gallery_pid + '/variant/' + variants[variant_key].variant_id + '/' + variants[variant_key].photos[photo_index].photo_path.large,
                                thumb_photo = product_gallery_assets + '/product/' + product_gallery_pid + '/variant/' + variants[variant_key].variant_id + '/' + variants[variant_key].photos[photo_index].photo_path.thumb;

                            gallery_html += '<a href="' + large_photo + '">';
                            gallery_html += '<img src="' + thumb_photo + '" width="70" height="70">';
                            gallery_html += '</a>';
                        }

                        gallery_html += '</div>';

                        product_gallery.html(gallery_html);
                        product_gallery.children().fotorama($.extend({}, fotorama_config, {
                            maxwidth: 560,
                            ratio: 580/400,
                            fit: 'contain'
                        }));
                    } else if (!is_default_product_gallery) {
                        is_default_product_gallery = true;

                        product_gallery.html(product_gallery_html);

                        if (product_gallery_html != '') {
                            product_gallery.children().fotorama($.extend({}, fotorama_config, {
                                maxwidth: 560,
                                ratio: 580/400,
                                fit: 'contain'
                            }));
                        }
                    }
                    */

                    enableBuyForm();
                    replaceProductUrl();
                } else {
                    /* if (!is_default_pb_form) {
                        is_default_pb_form = true;

                        // set default sku
                        if (product_sku != '') {
                            if (product_title_el.children('span').length) {
                                product_title_el.children('span').text(variants_lang.productCode + ' ' + product_sku);
                            } else {
                                product_title_el.append(' <span>' + variants_lang.productCode + ' ' + product_sku + '</span>');
                            }
                        } else {
                            product_title_el.children('span').remove();
                        }
                    } */

                    /* if (!is_default_product_gallery) {
                        is_default_product_gallery = true;

                        product_gallery.html(product_gallery_html);

                        if (product_gallery_html != '') {
                            product_gallery.children().fotorama($.extend({}, fotorama_config, {
                                maxwidth: 560,
                                ratio: 580/400,
                                fit: 'contain'
                            }));
                        }
                    } */

                    disableBuyForm();
                    replaceProductUrl();
                }

                product_variants_reset.removeClass('buy__variants-reset--hidden');
            } else {
                /* if (!is_default_product_gallery) {
                    is_default_product_gallery = true;

                    product_gallery.html(product_gallery_html);

                    if (product_gallery_html != '') {
                        product_gallery.children().fotorama($.extend({}, fotorama_config, {
                            maxwidth: 560,
                            ratio: 580/400,
                            fit: 'contain'
                        }));
                    }
                } */

                product_variants_selected = 0;
                product_variants_reset.addClass('buy__variants-reset--hidden');

                // enable all variant options
                product_variants_select.prop('disabled', false);

                // set price, variant id, disable buy button
                variant_el.val(0);
                price_el.text(number_format(price_base, 0, '.', ' '));

                disableBuyForm();
                replaceProductUrl();
            }
        });
    }
});