var cart,
    cart_btn,
    cart_qty,
    is_cart_xhr = false,
    cart_xhr,
    addToCart = function(e) {
        e.preventDefault();
        document.activeElement.blur();

        if ($(this).data('loading') === true) {
            return false;
        }

        var form = $(this),
            form_data = form.serialize();


        form.data('loading', true);
        form.find('button').prop('disabled', true);

        $.ajax({
            type: 'POST', 
            url: form.attr('action'),
            data: form_data,
            cache: false, 
            dataType: 'json',
            success: function(data) {
                form.data('loading', false);
                form.find('button').prop('disabled', false);
                
                if (data.error == true) {
                    // form.append('<div class="error-msg">' + data.errorCode + '</div>');
                    alert(data.errorCode);
                } else {
                    // window.location = form.attr('action');
                    
                    if (has_custom_scrollbar) {
                        cart.find('.simplebar-content').html(data.html);
                    } else {
                        cart.find('.cart__products').html(data.html);
                    }

                    cart.find('.cart__total-amount span').text(data.price);

                    // update cart button
                    cart_btn.text(data.total).parent().addClass('header__cart--products');
                    cart_qty.text(data.total);

                    // open popup
                    cartToggle();
                }
            },
            error: function(jqXHR, textStatus, errorThrown) { 
                form.data('loading', false);
                form.find('button').prop('disabled', false);

                alert('<div class="error-msg">Request error!</div>');
                console.log('Loader Error:\n' + textStatus + ' ' + errorThrown );
                console.log('Response:\n' + jqXHR.responseText);
            }
        });
    },
    cartToggle = function() {
        if (!is_sm_screen.matches) {
            toggleScrollLock();
        } else if (cart.hasClass('cart--hidden')) {
            scroll_top = $(window).scrollTop();
        }
        
        cart.toggleClass('cart--hidden');
        c.toggleClass('is-cart-open');

        if (is_sm_screen.matches && cart.hasClass('cart--hidden')) {
            $(window).scrollTop(scroll_top);
        }
    };
    initCart = function() {
        cart_btn = $('.header__cart-counter');
        cart_qty = $('.cart__title span');
        cart = $('#cart');

        cart_btn.parent().on('click', function(e) {
            if ($(this).hasClass('header__cart--checkout')) {
                return;
            }
            
            e.preventDefault();
            this.blur();

            if ($(this).hasClass('header__cart--products')) {
                cartToggle();
            }
        });

        var cart_items = cart.find('.cart__products'),
            cart_total = cart.find('.cart__total-amount span'),
            updateCart = function(params) {
                if (is_cart_xhr) {
                    return false;
                }

                is_cart_xhr = true;
                cart.addClass('loading');

                cart_xhr = $.ajax({
                    type: 'POST', 
                    url: cart_items.attr('action'), 
                    data: params, 
                    cache: false, 
                    dataType: 'json',
                    success: function(data) {
                        cart.removeClass('loading');
                        is_cart_xhr = false;

                        if (data.error) {
                            alert(data.errorCode);
                            return;
                        }

                        if (data.total) {
                            if (has_custom_scrollbar) {
                                cart_items.find('.simplebar-content').html(data.html);
                            } else {
                                cart_items.html(data.html);
                            }
                            cart_total.text(data.price);
                            cart_btn.text(data.total).parent().addClass('header__cart--products');
                            cart_qty.text(data.total);

                            $('#checkout').find('button').parent().find('.checkout__error').remove();

                            if (data.unavailable) {
                                $('#checkout').find('button').after('<div class="checkout__error">' + data.unavailable + '</div>');
                            }
                        } else if (data.total == 0) {
                            if (has_custom_scrollbar) {
                                cart_items.find('.simplebar-content').html(data.html);
                            } else {
                                cart_items.html(data.html);
                            }
                            cart_btn.text(data.total).parent().removeClass('header__cart--products');
                            cart_qty.text(data.total);
                            cartToggle();
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) { 
                        cart.removeClass('loading');
                        is_cart_xhr = false;
                        
                        // console.log('Loader Error:\n' + textStatus + ' ' + errorThrown );
                    }
                });
            };

        cart.on('click', '.cart__close, .cart__overlay', function(e) {
            e.preventDefault();
            cartToggle();
        });

        cart_items.on('submit', function(e) {
            e.preventDefault();
        });

        if (has_custom_scrollbar) {
            new SimpleBar(cart_items[0]);
        }

        cart_items.on('change', 'input[name="qty"]', function() {
            var value = parseInt($(this).val());

            if (value < 0 || isNaN(value)) {
                value = 1;
            }

            var params = {
                action: 'update',
                cart: {
                    product_id: $(this).data('id'),
                    variant_id: $(this).data('vid'),
                    option_id: $(this).data('oid'),
                    qty: value
                }
            };

            updateCart(params);
        });

        cart_items.on('click', 'button[data-id]', function(e) {
            e.preventDefault();

            var params = {
                action: 'remove',
                cart: {
                    product_id: $(this).data('id'),
                    variant_id: $(this).data('vid'),
                    option_id: $(this).data('oid')
                }
            };

            updateCart(params);
        });
    };

$(function() {
    $('.product-card').on('submit', 'form', addToCart);

	initCart();
});